var launchRemoved = false;
var loadingRemoved = false;
var host = "https://zzy.4plc.cn";
// var host = "http://10.0.0.68";
var mainUrl;
var headerPos, mainPos, footerH;
var registrationId;
var ajpush = null;
var aliasId = "";
var aliasNum = 0;

const privacy_right_key = "diteng.privacy-right_defaultProtocol";
const privacy_right_state = 'diteng.privacy-right_defaultState';
const name = "中智运";
const frameName = 'main';

// 页面初始化执行
apiready = function () {
    setHeaderFrameName(frameName);
    api.addEventListener({
        name: 'keyback'
    }, function (ret, err) {
        var fname = $api.attr($api.byId('header'), 'frameName');
        api.historyBack({
            frameName: fname
        }, function (ret, err) {
            if (!ret.status) {
                api.closeWidget();
            }
        });
    });
    initPrivacy();
}

// 初始化用户政策与隐私协议
function initPrivacy() {
    let value = localStorage.getItem(privacy_right_key);
    if (value) {
        main();
    } else {
        api.openFrame({
            name: frameName,
            url: "./html/privacy-right.html",
            pageParam: {
                host: host
            }
        })
    }
}

// 绑定极光推送与首页地址
function main() {
    var deviceId = api.deviceId;
    var sType = api.systemType;
    ajpush = api.require('ajpush');

    // var header = $api.byId('header');
    var footer = $api.byId('footer');
    // 适配iOS 7+，Android 4.4+状态栏
    // $api.fixStatusBar(header);
    $api.fixTabBar(footer);
    footerH = $api.offset(footer).h;

    var main = $api.byId('main');
    // headerPos = $api.offset(header);
    mainPos = $api.offset(main);
    deviceId = deviceId.replace(/-/g, "");
    deviceId = deviceId.replace(/:/g, "");

    if (sType == "android") {
        mainUrl = host + '/public/WebDefault?CLIENTID=n_' + deviceId + '&device=android';
        aliasId = "n_" + deviceId;
    } else {
        mainUrl = host + '/public/WebDefault?CLIENTID=i_' + deviceId + '&device=iphone';
        aliasId = "i_" + deviceId;
    }
    console.log(mainUrl)
    initApp(mainUrl);
    $api.fixTabBar($api.byId('footer'));

    ajpush.init(function (ret) {
        if (ret && ret.status) {
            console.log("极光推送注册成功");
        }
    });

    var param = { alias: aliasId };
    // 绑定用户别名为极光id。服务端可以指定别名进行消息推送
    ajpush.bindAliasAndTags(param, function (ret) { });

    ajpush.getRegistrationId(function (ret, err) {
        if (ret.id) {
            registrationId = ret.id;
            api.setPrefs({
                key: 'registrationId',
                value: ret.id
            });
            setAlias(aliasId);
        }
    });

    // 监听用户网络中断事件
    api.addEventListener({
        name: 'offline'
    }, function (ret) {
        if (confirm("你访问的页面走丢了，请检查网络是否正常")) {
            offline();
        } else {
            api.closeWidget({
                silent: true
            });
        }
    });

    // 监听Android返回按钮事件
    api.addEventListener({
        name: 'keyback'
    }, function (ret, err) {
        headerLeft();
    });
}

// 网络断线处理函数
function offline() {
    window.localStorage.setItem("apiInPage", 1)
    api.openFrame({
        name: "error",
        url: "./error/error.html"
    })
}

// 设置别名
function setAlias(aliasId) {
    var param = { alias: aliasId };
    ajpush.setAlias(param, function (ret) {
        var statusCode = ret.statusCode;
        if (statusCode === 6002 && aliasNum < 30) {
            aliasNum++;
            setTimeout('setAlias("' + aliasId + '")', 15 * 1000);
        }
    });
}

// 初始化App
function initApp(mainUrl) {
    api.openFrame({
        name: frameName,
        url: mainUrl,
        allowEdit: true,
        usewkWebview: true,
        scrollEnabled: false,
        hScrollBarEnabled: false,
        scaleEnabled: false
    });
}

function showFrame() {
    var frames = api.frames();
    // 手动实现兼容App状态栏
    let safeAreaTop = api.safeArea.top;
    let headerHeight = $api.offset($api.byId('header')).h;
    let paddingTop = $api.cssVal($api.byId('header'), `padding-top`).slice(0, -2);
    if (parseFloat(paddingTop) == 0)
        $api.css($api.byId('header'), `padding-top: ${safeAreaTop}px`);
    for (var key in frames) {
        api.setFrameAttr({
            name: frames[key].name,
            rect: {
                marginTop: parseFloat(paddingTop) == 0 ? headerHeight + safeAreaTop : headerHeight,
                marginBottom: footerH
            },
            hidden: false,
            usewkWebview: true
        });
    }
}

// 头部弹层新建窗口与设置界面
function newFrame(mainUrl) {
    var name = 'main' + new Date().getTime();
    api.openFrame({
        name: name,
        url: mainUrl,
        allowEdit: true,
        usewkWebview: true,
        scrollEnabled: false,
        hScrollBarEnabled: false,
        scaleEnabled: false
    });
    showFrame();
    setTimeout(function () {
        hideHeader(false);
    }, 100);
    setHeaderFrameName(name);
}

// 关闭所有打开的frame
function closeAllFrames() {
    var frames = api.frames();
    for (var i = 0; i < frames.length; i++) {
        api.closeFrame({
            name: frames[i].name
        })
    }
    initApp(mainUrl);
}

// 头部是否隐藏（登录页面不展示头部）
function hideHeader(flog) {
    document.querySelector("#header").style.display = flog ? "none" : "flex";
    showFrame();
}

// 设置App头部标题(主体内容为空，兼容4PL管家系统中出现地藤的帐套)
function setHeaderTitle(title) {
    
}

// 设置header的frameName属性
function setHeaderFrameName(name) {
    $api.attr($api.byId('header'), 'frameName', name);
}

// 关闭名字为当前header中frameName属性的frame
function closeFrame() {
    var name = $api.attr($api.byId('header'), 'frameName');
    api.closeFrame({ name: name });
}

// 历史回退处理函数
function goHistoryBack() {
    var fname = $api.attr($api.byId('header'), 'frameName');
    api.historyBack({
        frameName: fname
    }, function (ret, err) {
        if (!ret.status) {
            if (api.frames().length > 1) {
                api.execScript({
                    frameName: fname,
                    script: 'window.ApiCloud.CloseFrame();'
                })
            } else {
                api.execScript({
                    frameName: fname,
                    script: 'window.ApiCloud.historyBack();'
                })
            }
        } else {
            if (fname == "main") {
                setTimeout(function () {
                    api.sendEvent({
                        name: 'historyBack',
                    })
                }, 0);
            }
        }
    });
}

// 头部左侧返回按钮点击事件（已停用）
function headerLeft(dom) {
    if (dom && dom.classList.contains('close')) {
        dom.classList.remove('close');
        dom.classList.add('back');
        closeAllFrames();
        hideHeader(true);
        api.openFrame({
            name: "privacy-right",
            url: "./html/privacy-right.html",
            pageParam: {
                host: host
            }
        })
    } else {
        var name = $api.attr($api.byId('header'), 'frameName');
        api.sendEvent({
            name: 'headerEvent',
            extra: {
                type: 'left',
                frameName: name,
                headerHeight: $api.offset($api.byId('header')).h
            }
        })
    }
}

// 头部中间区域点击事件
function headerCenter() {
    var name = $api.attr($api.byId('header'), 'frameName');
    api.sendEvent({
        name: 'headerEvent',
        extra: {
            type: 'center',
            frameName: name,
            headerHeight: $api.offset($api.byId('header')).h
        }
    })
}

// 头部右侧menu图标点击事件
function headerRight() {
    var name = $api.attr($api.byId('header'), 'frameName');
    api.sendEvent({
        name: 'headerEvent',
        extra: {
            type: 'right',
            frameName: name,
            headerHeight: $api.offset($api.byId('header')).h
        }
    })
}